﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafUtilityMgmt;

namespace AtafCommonCore.AtafAppConfigSettingMgmt
{
  public class AtafPropertySettingHandler
  {

    private static string atafCoreAppConfig = "AtafCoreApp.config";
    private static Configuration atafAppSettingsConfig;

    public static int ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC;
    public static string ATAF_WEB_GUI_DRIVER_TO_RUN;
    public static string ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH;
    public static string ATAF_WEB_DESKTOP_GUI_PERSISTED_SCREENSHOT_BASE_PATH;
    public static int ATAF_WEB_GUI_DEMO_SLOW_DOWN_BROWSER_MSEC;
    public static int ATAF_WEB_DESKTOP_GUI_PAGE_LOAD_STRATEGY;

    private static void setConfigValues(Configuration appSettingsConfig)
    {
      atafAppSettingsConfig = appSettingsConfig;
      ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC = int.Parse(getValue("ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC"));
      ATAF_WEB_GUI_DRIVER_TO_RUN = getValue("ATAF_WEB_GUI_DRIVER_TO_RUN");
      ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH = getValue("ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH");
      ATAF_WEB_DESKTOP_GUI_PERSISTED_SCREENSHOT_BASE_PATH = getValue("ATAF_WEB_DESKTOP_GUI_PERSISTED_SCREENSHOT_BASE_PATH");
      ATAF_WEB_GUI_DEMO_SLOW_DOWN_BROWSER_MSEC = int.Parse(getValue("ATAF_WEB_GUI_DEMO_SLOW_DOWN_BROWSER_MSEC"));
      ATAF_WEB_DESKTOP_GUI_PAGE_LOAD_STRATEGY = int.Parse(getValue("ATAF_WEB_DESKTOP_GUI_PAGE_LOAD_STRATEGY"));
    }

    private static string getValue(string configKey)
    {
      if (atafAppSettingsConfig.AppSettings.Settings[configKey] != null)
      {
        return atafAppSettingsConfig.AppSettings.Settings[configKey].Value.Replace("__", "");
      }
      else
      {
        var property = new AtafPropertySettingHandler().GetType().GetFields().First(p => p.Name.Equals(configKey));
        return property.GetValue(property).ToString();
      }
    }

    public static void ReadAndPrintAtafCoreAppSettings(List<Configuration> allAppSettingsConfig)
    {
      string tempDirForAtafAppConfig = AtafCommonHelpers.GetProjectDirectory() + @"\Temp";
      if (!Directory.Exists(tempDirForAtafAppConfig))
      {
        Directory.CreateDirectory(tempDirForAtafAppConfig);
      }

      string atafCoreAppConfigPath = AtafFileHelper.GetReourceStreamAsFile(AtafFileHelper.GetResourceFileAsStream(atafCoreAppConfig), tempDirForAtafAppConfig + @"\" + atafCoreAppConfig).FullName;

      ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
      fileMap.ExeConfigFilename = atafCoreAppConfigPath;
      var atafAppConfig = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
      
      //Set the values for ATAF config from AtafCoreApp.config
      setConfigValues(atafAppConfig);
      
      //Override the values for ATAF config from AtafCoreApp.config
      foreach (var appSettingsConfig in allAppSettingsConfig)
      {
        if (appSettingsConfig.AppSettings.Settings.AllKeys.Length > 0)
        {
          setConfigValues(appSettingsConfig);
        }
      }

      AtafPropertySettingHandler atafPropertySettingHandler = new AtafPropertySettingHandler();
      LogAllPropertySettings(atafPropertySettingHandler);
    }



    public static List<Configuration> GetAllSolutionAppConfigSettings()
    {
      AtafLog.Info(AtafLog.AddHaschTagsToText("Load enviroment properties"));
      string[] allAppConfigFiles = Directory.GetFiles(AtafCommonHelpers.GetSolutionDirectory(), "*taf*.config", SearchOption.AllDirectories);
      var allAppConfigFilesFiltered = allAppConfigFiles.Where(f => !f.Contains("Debug") && !f.Contains(atafCoreAppConfig)).ToList();

      List<Configuration> configurationList = new List<Configuration>();

      foreach (var file in allAppConfigFilesFiltered)
      {

        try
        {
          ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
          fileMap.ExeConfigFilename = file;
          configurationList.Add(ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None));
        }
        catch (Exception e)
        {
          AtafLog.Debug(e.Message);

        }
      }

      ReadAndPrintAtafCoreAppSettings(configurationList);
      return configurationList;
    }

    public static void AssignPropertySettingsToObject()
    {
      AtafPropertySettingHandler atafPropertySettingHandler = new AtafPropertySettingHandler();
      var propertyInfos = atafPropertySettingHandler.GetType().GetFields();
      //GetProperties();
      foreach (var property in propertyInfos)
      {
        AtafLog.Info(property.Name);
      }
    }

    public static void LogAllPropertySettings(Object o)
    {
      var allProperties = o.GetType().GetFields();
      foreach (var property in allProperties)
      {
        AtafLog.Info(property.Name, property.GetValue(property).ToString());
      }
    }

  }
}