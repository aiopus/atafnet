﻿using AtafCommonCore.AtafLogMgmt;
using NUnit.Framework;
using System;
using AtafCommonCore.AtafNUnitMgmt;
using AtafCommonCore.AtafUtilityMgmt;

namespace AtafCommonCore.Test
{
  [AtafNUnitTestManager]
  public class AtafLogTest
  {

    [OneTimeSetUp]
    public void OneTimeSetUpForClass()
    {
      AtafLog.TestClassStart();
    }

    [SetUp]
    public void SetUpTestCase()
    {
      AtafLog.TestCaseStart();

    }

    [Test]
    public void Given_Log4N_Configured_When_Test_Run_Then_Info_Log_Is_Logged()
    {
      AtafLog.Info(AtafEncryptionDecryption.EncryptString("Test", "test"));
      AtafLog.Info("This is a simple log to console and file");
      AtafLog.Info(AtafEncryptionDecryption.DecryptString("+hMSxqvNkjpPt31PkUTChQ==", "test"));
    }

    [TearDown]
    public void TearDownCase()
    {
      AtafLog.TestCaseEnd();
    }
  }
}