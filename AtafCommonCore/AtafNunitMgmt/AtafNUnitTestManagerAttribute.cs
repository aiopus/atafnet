﻿using AtafCommonCore.AtafLogMgmt;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using AtafCommonCore.AtafAppConfigSettingMgmt;
using AtafCommonCore.AtafNunitMgmt;
using AtafCommonCore.AtafUtilityMgmt;
using log4net.Config;
using TestCommand = NUnit.Framework.Internal.Commands.TestCommand;

namespace AtafCommonCore.AtafNUnitMgmt
{
  public class AtafNUnitTestManagerAttribute : PropertyAttribute
  {
    public AtafNUnitTestManagerAttribute()
    {
      //Fetch config file AtafLog4net.config
      XmlConfigurator.Configure(AtafFileHelper.GetResourceFileAsStream("AtafLog4Net.config"));
      AtafLog.Debug();
    }

    

  }

 

}