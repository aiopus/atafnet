﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;

namespace AtafCommonCore.AtafNunitMgmt
{
  public abstract class TestCommand
  {
    public TestCommand(NUnit.Framework.Internal.Test test)
    {
      this.Test = test;
    }

    public NUnit.Framework.Internal.Test Test { get; private set; }

    public abstract TestResult Execute(TestExecutionContext context);
  }
}
