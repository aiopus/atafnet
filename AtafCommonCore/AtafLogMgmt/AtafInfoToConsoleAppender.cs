﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.ObjectRenderer;
using log4net.Appender;
using log4net.Core;
using log4net.Util;
using log4net.Filter;
using log4net.Repository;
using log4net.Config;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AtafCommonCore.AtafLogMgmt
{
  /// <summary>
  /// Implement AppenderSkeleten, Catch the logging message, if level info the message will be printed to Console as System dignostisc debug
  /// in order to make the message printed to a console
  /// </summary>
  public class AtafInfoToConsoleAppender : AppenderSkeleton
  {
    protected override void Append(LoggingEvent loggingEvent)
    {
      Level logLevel = Level.Error;
      switch (loggingEvent.Level.Name)
      {
        case "DEBUG":
          logLevel = Level.Debug;
          break;
        case "WARN":
        case "INFO":
          logLevel = Level.Info;
          break;
        case "ERROR":
          logLevel = Level.Error;
          break;
        case "FATAL":
          logLevel = Level.Critical;
          break;
      }
      string logEv = RenderLoggingEvent(loggingEvent);

      System.Diagnostics.Debug.Write(logEv.Substring(logEv.IndexOf(":") + 2));
      System.Console.Write(logEv.Substring(logEv.IndexOf(":") + 2));
    }
  }
}
