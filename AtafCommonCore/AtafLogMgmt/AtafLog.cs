﻿using AtafCommonCore.AtafUtilityMgmt;
using log4net;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Remote;
using System;
using System.Diagnostics;
using AtafCommonCore.AtafAppConfigSettingMgmt;


namespace AtafCommonCore.AtafLogMgmt
{

  public class AtafLog
  {


    public static RemoteWebDriver RunningDriver { get; set; }

    public static FlaUI.Core.Application RunningApplication { get; set; }

    public static bool IsPreviousTestCaseSucceeded { get; set; }
    public static readonly ILog logger = LogManager.GetLogger(typeof(AtafLog));
    static int numberOfFailedTests;
    static int numberOfExecutedTests;
    static int numberOfSucceededTests;
    static bool isDesktopApplicationClosed;


    public bool IsDesktopApplicationClosed
    {
      get { return AtafLog.isDesktopApplicationClosed; }
      set { AtafLog.isDesktopApplicationClosed = value; }
    }



    public static int NumberOfSucceededTests
    {
      get { return AtafLog.numberOfSucceededTests; }
      set { AtafLog.numberOfSucceededTests = value; }
    }

    public static int NumberOfExecutedTests
    {
      get { return AtafLog.numberOfExecutedTests; }
      set { AtafLog.numberOfExecutedTests = value; }
    }

    static int totalNumberOfTestToRun;

    public static int TotalNumberOfTestsToRun
    {
      get { return totalNumberOfTestToRun; }
      set { totalNumberOfTestToRun = value; }
    }



    public static void Debug(params String[] debugMessageToLog)
    {
      logger.Debug(ArrayToStringMessage(debugMessageToLog));
    }

    public static void Info(params String[] infoMessageToLog)
    {
      logger.Info(ArrayToStringMessage(infoMessageToLog));
    }

    public static void TestStep(params String[] testStepMessageToLog)
    {
      logger.Info(ArrayToStringMessage(testStepMessageToLog));
    }

    public static void Warning(params String[] warningMessageToLog)
    {
      logger.Warn(ArrayToStringMessage(warningMessageToLog));

    }

    private static String ArrayToStringMessage(params String[] str)
    {
      String messageToLog = "";
      for (int i = 0; i < str.Length; i++)
      {
        messageToLog = messageToLog + " :" + str[i];
      }

      return GetClassAndMethodName() + messageToLog;
    }


    public static void TestClassStart()
    {
      AtafLog.IsPreviousTestCaseSucceeded = true;
      logger.Info(AddHaschTagsToText("TEST CLASS " + getTestClassName() + " STARTS"));
    }

    public static void TestClassEnd()
    {
      logger.Info(AddHaschTagsToText("TEST CLASS " + getTestClassName() + " ENDS"));
    }

    public static void TestCaseStart()
    {

      logger.Info(AddHaschTagsToText("TEST CASE " + TestContext.CurrentContext.Test.Name.ToUpper() + " STARTS"));
    }

    public static void TestCaseEnd(string atafBasePathScreenshot=null)
    {
      AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH = atafBasePathScreenshot == null ? AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH : atafBasePathScreenshot;
      string testResult = "SUCCEEDED";
      string message = "";
      String webDriverPageSource = "";
      if (TestContext.CurrentContext.Result.Outcome == ResultState.Error ||
          TestContext.CurrentContext.Result.Outcome == ResultState.Failure)
      {
        IsPreviousTestCaseSucceeded = false;
        --numberOfSucceededTests;
        ++numberOfFailedTests;
        testResult = "FAILED";
        message = "\n*****************ERROR MESSAGE *****************n" +
                  TestContext.CurrentContext.Result.Message;
        if (RunningDriver != null)
        {
          
            AtafCommonHelpers.TakeWebDriverScreenShot(RunningDriver);
            webDriverPageSource = RunningDriver.PageSource;
        }
        else if (RunningApplication != null)
        {
          AtafCommonHelpers.TakeWebDriverScreenShot(null, RunningApplication);
        }
      }
      else
      {
        IsPreviousTestCaseSucceeded = true;
        ++numberOfSucceededTests;
      }

      String failedTests = numberOfFailedTests == 0 ? "" : "\n************ Total number of failed tests: " + numberOfFailedTests + " ***********";
      logger.Info(AddHaschTagsToText("\n*****************************" + testResult + "***********************************" +
                  "\n************ Total number of executed tests : " + ++numberOfExecutedTests + "(" +
                  totalNumberOfTestToRun + ") *********" + failedTests +
                  "\n**************TEST CASE " + TestContext.CurrentContext.Test.MethodName.ToUpper() +
                  " ENDS**************" +
                  message +
                  "\n***********************************************************\n")
      + webDriverPageSource
      );
      
    }

    public static void TestCaseFailed()
    {
      logger.Info(AddHaschTagsToText("TEST CASE, " + TestContext.CurrentContext.Test.MethodName + " FAILED"));
    }

    public static void TestCaseErrorMessage(Exception e)
    {
      logger.Info(AddHaschTagsToText("ERROR MESSGE: " + e.Message + "\n STACK TRACE: " + e.StackTrace));

    }

    public static string AddHaschTagsToText(String textToAddHashTagsTo)
    {
      String haschTags = "\n";
      for (int i = 0; i < textToAddHashTagsTo.Length + 13; i++)
      {
        haschTags = haschTags + "#";
      }
      haschTags = haschTags + "\n";
      return haschTags + "#####  " + textToAddHashTagsTo + " #####" + haschTags;
    }

    private static string GetClassAndMethodName()
    {
      string className = null;
      string methodName = null;
      StackTrace stackTrace = new StackTrace(true); // get call stack
      return stackTrace.GetFrame(3).GetMethod().DeclaringType.Name.ToUpper().PadRight(60) +
             stackTrace.GetFrame(3).GetMethod().Name;
    }


    private static string getTestClassName()
    {
      return TestContext.CurrentContext.Test.ClassName.Substring(
        TestContext.CurrentContext.Test.ClassName.LastIndexOf(".") + 1);
    }


  }
}