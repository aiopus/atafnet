﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using AtafWebGuiCore.WebDriver;

namespace AtafWebGuiCore.Common
{
  /// <summary>
  /// Instatiate the needed class for interacting with ATAF RunningDriver functionalities
  /// </summary>
  public class AtafInstantiateDriverClass
  {
    protected static  AtafWebDriverFindElement atafWebDriverFindElement;
    protected static AtafWebDriverFactory atafWebDriverFactory;
    protected static AtafWebDriverOperateOnElement atafWebDriverOperateOnElement;

    /// <summary>
    /// Instantiate class:
    /// AtafWebDriverFindElemen
    /// AtafWebDriverFactory
    /// AtafWebDriverOperateOnElement
    /// This classes are need in order to start a driver, find a webelement and operate on the element
    /// </summary>
    public void InstantiateWebDriver()
    {
      AtafLog.Info();
      if (atafWebDriverFactory == null)
      {
        atafWebDriverFactory = new AtafWebDriverFactory();
        atafWebDriverFindElement = new AtafWebDriverFindElement();
        atafWebDriverOperateOnElement = new AtafWebDriverOperateOnElement();
        AtafWebDriverOperateOnElement.AtafWebDriverFindElement = atafWebDriverFindElement;
      }
    }

  }
}
