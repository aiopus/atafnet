﻿using AtafCommonCore.AtafLogMgmt;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using AtafCommonCore.AtafAppConfigSettingMgmt;
using SeleniumExtras.WaitHelpers;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace AtafWebGuiCore.WebDriver
{
  public class AtafWebDriverFindElement : AtafWebDriverFactory
  {

    public IWebElement GetWebElement(string pageNameToSwitchTo, string frameNameOrIndexToSwitchTo, By by, Func<IWebDriver, IWebElement> expectedConditions = null)
    {
      return GetWebElement(pageNameToSwitchTo, frameNameOrIndexToSwitchTo, by, AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC , expectedConditions);
    }

    public  IWebElement GetWebElement(string pageNameToSwitchTo, string frameNameOrIndexToSwitchTo, By by,  int maxTimeToWaitInMSec, Func<IWebDriver, IWebElement> expectedConditions = null)
    {
      AtafLog.Debug(pageNameToSwitchTo, frameNameOrIndexToSwitchTo, by.ToString());
      SwitchToFrameOrWindowByName(AtafDriver, pageNameToSwitchTo, frameNameOrIndexToSwitchTo);
      expectedConditions = expectedConditions == null ? ExpectedConditions.ElementIsVisible(by) : expectedConditions;
      var webDriverWait = new WebDriverWait(new SystemClock(), AtafDriver, TimeSpan.FromMilliseconds(maxTimeToWaitInMSec), TimeSpan.FromMilliseconds(500));
      //Demo purpose, slow down the browser
      Thread.Sleep(AtafPropertySettingHandler.ATAF_WEB_GUI_DEMO_SLOW_DOWN_BROWSER_MSEC);
      return webDriverWait.Until(expectedConditions);
    }

    public List<IWebElement> GetWebElements(string pageNameToSwitchTo, string frameNameOrIndexToSwitchTo, By by, int maxTimeToWaitInMSec, Func<IWebDriver, IWebElement> expectedConditions = null)
    {
      AtafLog.Debug(pageNameToSwitchTo, frameNameOrIndexToSwitchTo, by.ToString());
      SwitchToFrameOrWindowByName(AtafDriver, pageNameToSwitchTo, frameNameOrIndexToSwitchTo);
      expectedConditions = expectedConditions == null ? ExpectedConditions.ElementIsVisible(by) : expectedConditions;
      var webDriverWait = new WebDriverWait(new SystemClock(), AtafDriver, TimeSpan.FromMilliseconds(maxTimeToWaitInMSec), TimeSpan.FromMilliseconds(500));
      //Demo purpose, slow down the browser
      Thread.Sleep(AtafPropertySettingHandler.ATAF_WEB_GUI_DEMO_SLOW_DOWN_BROWSER_MSEC);
      webDriverWait.Until(expectedConditions);
      return AtafDriver.FindElements(by).ToList();
    }

    public IWebElement GetWebElementWithoutException(string pageNameToSwitchTo, string frameNameOrIndexToSwitchTo, By by,
      int maxTimeToWaitInMSec, Func<IWebDriver, IWebElement> expectedConditions = null)
    {
      AtafLog.Debug(pageNameToSwitchTo, frameNameOrIndexToSwitchTo, by.ToString());
      SwitchToFrameOrWindowByName(AtafDriver, pageNameToSwitchTo, frameNameOrIndexToSwitchTo);
      expectedConditions = expectedConditions == null ? ExpectedConditions.ElementExists(by) : expectedConditions;
      var webDriverWait = new WebDriverWait(new SystemClock(), AtafDriver,TimeSpan.FromMilliseconds(maxTimeToWaitInMSec), TimeSpan.FromMilliseconds(500));
      try
      {
        return webDriverWait.Until(expectedConditions);
      }
      catch (Exception e)
      {
        return null;
      }
    }


    /// <summary>
    /// Modify the driver if needed, by switching page or frame
    /// The switching is needed incase the element is placed in a different frame or page than the defualt ones.
    /// </summary>
    /// <param name="driver"></param>
    /// <param name="pageNameToSwitchTo"></param>
    /// <param name="frameNameOrIndexToSwitchTo"></param>
    /// <returns></returns>
    private static RemoteWebDriver SwitchToFrameOrWindowByName(RemoteWebDriver driver, string pageNameToSwitchTo, string frameNameOrIndexToSwitchTo)
    {
      //Go back to default page and frame
      if (frameNameOrIndexToSwitchTo == null && pageNameToSwitchTo == null)
      {
        try
        {
          driver.SwitchTo().DefaultContent();
        }
        catch
        {
          //DONOTHING TODO
        }
      }
      //only frame switch
      else if (frameNameOrIndexToSwitchTo != null && pageNameToSwitchTo == null)
      {
        try
        {
          driver.SwitchTo().DefaultContent();
        }
        catch
        {
          //DONOTHING TODO
        }

        try
        {
          driver.SwitchTo().Frame(int.Parse(frameNameOrIndexToSwitchTo));

        }
        catch (Exception e)
        {
          driver.SwitchTo().Frame(frameNameOrIndexToSwitchTo);
        }
      }
      //Only Window Switch
      else if (frameNameOrIndexToSwitchTo == null && pageNameToSwitchTo != null)
      {
        driver = SwitchWindowByTitle(driver, pageNameToSwitchTo);
      }
      //Both frame and window Switch
      else if (frameNameOrIndexToSwitchTo != null && pageNameToSwitchTo != null)
      {
        driver = SwitchWindowByTitle(driver, pageNameToSwitchTo);
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(frameNameOrIndexToSwitchTo);
      }

      return driver;
    }


    /// <summary>
    /// Switch window
    /// The new window is identified by name
    /// </summary>
    /// <param name="driver"></param>
    /// <param name="pageNameToSwitchTo"></param>
    /// <returns></returns>
    private static RemoteWebDriver SwitchWindowByTitle(RemoteWebDriver driver, string pageNameToSwitchTo)
    {
      AtafLog.Debug(driver.GetType().FullName, pageNameToSwitchTo);
      IReadOnlyCollection<string> windowList = driver.WindowHandles;
      foreach (string pageName in windowList)
      {
        string title = driver.SwitchTo().Window(pageName).Title;
        if (title.Replace(" ", "").ToLower().Equals(pageNameToSwitchTo.Replace(" ", "").ToLower()))
        {
          break;
        }
      }
      return driver;
    }
  }
}
