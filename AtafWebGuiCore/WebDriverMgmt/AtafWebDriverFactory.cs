﻿using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafUtilityMgmt;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using AtafCommonCore.AtafAppConfigSettingMgmt;
using log4net.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AtafWebGuiCore.WebDriver
{
  public class AtafWebDriverFactory
  {
    private static RemoteWebDriver atafDriver;
    public static string AtafDriverProcessId { get; set; }

    public static RemoteWebDriver AtafDriver
    {
      get => atafDriver;
      set => atafDriver = value;
    }

    enum DriverType
    {
      ChromeDriver,
      IExplorerDriver,
      FireFoxDriver
    }

    public static void StartWebDriverApplication(string typeOfDriverToRun)
    {
      List<DriverType> driverTypesList = Enum.GetValues(typeof(DriverType)).Cast<DriverType>().ToList();
      DriverType driverType =
        (DriverType) driverTypesList.FindIndex(p => typeOfDriverToRun.ToLower().Contains(p.ToString().ToLower()));
      switch (driverType)
      {
        case DriverType.IExplorerDriver:
          StartIEDriver();
          break;
        case DriverType.ChromeDriver:
          StartChromeDriver();
          break;
      }


    }


    private static void StartChromeDriver()
    {
      AtafLog.Debug();
      ChromeOptions options = new ChromeOptions();
      options.AcceptInsecureCertificates = true;
      
      options.AddArguments("enableNetwork=false");
      options.AddArguments("--no-sandbox");
      options.AddArguments("disable-features=NetworkService");
      options.AddArguments("--log-level=4");
      options.AddArguments("--silent");
      options.AddArguments("--disable-browser-side-navigation");
      options.AddArguments("--disable-features=VizDisplayCompositor");
      options.AddArguments("disable-gpu");
      options.AddArguments("--disable-dev-shm-usage");
      options.SetLoggingPreference(LogType.Profiler, LogLevel.Debug);
      options.SetLoggingPreference(LogType.Browser, LogLevel.Debug);
      options.SetLoggingPreference(LogType.Client, LogLevel.Debug);
      options.SetLoggingPreference(LogType.Driver, LogLevel.Debug);
      options.SetLoggingPreference(LogType.Server, LogLevel.Debug);

      switch (AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_PAGE_LOAD_STRATEGY)
      {
        case 0:
          options.PageLoadStrategy = PageLoadStrategy.Default;
          break;
        case 1:
          options.PageLoadStrategy = PageLoadStrategy.Normal;
          break;
        case 2:
          options.PageLoadStrategy = PageLoadStrategy.Eager;
          break;
        case 3:
          options.PageLoadStrategy = PageLoadStrategy.None;
          break;
        default:
          options.PageLoadStrategy = PageLoadStrategy.Normal;
          break;
      }
        


      try
      {
        AtafDriver = new ChromeDriver(options);
      }
      catch (Exception e)
      {
        throw new Exception("The RunningDriver could not be started: ", e);
      }

      AtafDriver.Manage().Window.Maximize();
      AtafLog.RunningDriver = AtafDriver;
    }

    private static void StartIEDriver()
    {
      AtafLog.Debug();
      var options = new InternetExplorerOptions
      {
        IntroduceInstabilityByIgnoringProtectedModeSettings = false,
        RequireWindowFocus = true,


      };

      try
      {
        AtafDriver = new InternetExplorerDriver(options);
      }
      catch (Exception e)
      {
        throw new Exception("The RunningDriver could not be started: ", e);
      }

      AtafDriver.Manage().Window.Maximize();
      AtafLog.RunningDriver = AtafDriver;
    }

    public static void GoToSite(string uRLForSiteToOpen)
    {
      AtafLog.Debug();
      int maxTimeToWait = DateTime.Now.Millisecond + AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC;
      AtafDriver.Navigate().GoToUrl(@uRLForSiteToOpen);

      while (!AtafDriver.Url.Contains("http") && DateTime.Now.Millisecond < maxTimeToWait)
      {
        Thread.Sleep(3000);
        AtafDriver.Navigate().GoToUrl(@uRLForSiteToOpen);
      }
    }

    public static void CloseWebDriver()
    {
      AtafLog.Debug();
      if (AtafDriver != null)
      {
        AtafDriver.Close();
        AtafDriver.Quit();
        AtafDriver = null;
        AtafProcessMgmt.killProcessByNames(AtafPropertySettingHandler.ATAF_WEB_GUI_DRIVER_TO_RUN);
      }
    }

  }
}
