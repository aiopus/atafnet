﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using AtafCommonCore.AtafAppConfigSettingMgmt;
using AtafCommonCore.AtafUtilityMgmt;
using AutoIt;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace AtafWebGuiCore.WebDriver
{
  public class AtafWebDriverOperateOnElement
  {
    public static AtafWebDriverFindElement AtafWebDriverFindElement;


    public void InsertInToFieldByLocator(By byLocator, string textToInsert)
    {
      InsertInToFieldByLocator(null, byLocator, textToInsert);
    }

    public void InsertInToFieldByLocator(String frameNameOrIndexToSwitchTo, By byLocator, string textToInsert)
    {

      AtafWebDriverFindElement.GetWebElement(null, frameNameOrIndexToSwitchTo, byLocator).Clear();
      AtafWebDriverFindElement.GetWebElement(null, frameNameOrIndexToSwitchTo, byLocator).SendKeys(textToInsert);
    }


    public void ClickElementByLocator(By byLocator)
    {
      ClickElementByLocator(null, byLocator);
    }

    public void ClickElementByLocator(String frameNameOrIndexToSwitchTo, By byLocator)
    {
      AtafWebDriverFindElement.GetWebElement(null, frameNameOrIndexToSwitchTo, byLocator).Click();
    }

    public void SendTabKeyByLocator(By byLocator)
    {
      AtafWebDriverFindElement.GetWebElement(null, null, byLocator).SendKeys(Keys.Tab);
    }

    public bool ExistElementByLocator(By byLocator, int maxTimeToWaitInMSec=1000)
    {
      if (AtafWebDriverFindElement.GetWebElementWithoutException(null, null, byLocator, maxTimeToWaitInMSec, null) != null)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public void ActivateCheckBoxByLocatorThrowExceptionIfActivationFails(By byLocator)
    {
      AtafCommonHelpers.WaitWhileFalseAndMaxTime(() => AtafWebDriverFindElement.GetWebElement(null, null, byLocator).Selected,
        () => AtafWebDriverFindElement.GetWebElement(null, null, byLocator).Click());
      
    }

    public void ActivateCheckBoxsByLocator(By byLocator)
    {
      List<IWebElement> checkBoxElements = AtafWebDriverFindElement.GetWebElements(null, null, byLocator, AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC);
      foreach (var checkBoxElement in checkBoxElements)
      {
        AtafCommonHelpers.WaitWhileFalseAndMaxTime(() => checkBoxElement.Selected, () => checkBoxElement.Click());
      }
    }

    public void ActivateCheckBoxByLocator(By byLocator)
    {
      IWebElement checkBoxElement = AtafWebDriverFindElement.GetWebElement(null, null, byLocator, AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC);
      if( !checkBoxElement.Selected)
      {
        checkBoxElement.Click();
      }
    }

    public void SelectFromDropDownByLocatorAndValue(By byLocator, string valueToSelect)
    {
      var webElement = AtafWebDriverFindElement.GetWebElement(null, null, byLocator);
      var select = new SelectElement(webElement);
      select.SelectByValue(valueToSelect);
    }

    public void SelectFromDropDownByLocatorAndIndex(By byLocator, int indexOfTheElementToSelect)
    {
      var webElement = AtafWebDriverFindElement.GetWebElement(null, null, byLocator);
      var select = new SelectElement(webElement);
      select.SelectByIndex(indexOfTheElementToSelect);
    }

    public void SelectFromDropDownByLocatorAndContainsText(By byLocator, string textToSelect)
    {
      SelectFromDropDownByLocatorAndContainsText(null, byLocator, textToSelect);
    }

    public void SelectFromDropDownByLocatorAndContainsText( String frameNameOrIndexToSwitchTo,By byLocator, string textToSelect)
    {
      var webElement = AtafWebDriverFindElement.GetWebElement(null, frameNameOrIndexToSwitchTo, byLocator);
      var select = new SelectElement(webElement);
      IList<IWebElement> webElements = select.Options;
      int indexToSelect;
      for (int i = 0; i < webElements.Count; i++)
      {
        if (webElements[i].Text.Contains(textToSelect))
        {
          select.SelectByIndex(i);
          return;
        }
      }
      throw new Exception("The text, " + textToSelect + " was not found in the dropdown, " + byLocator);
    }
    public void SelectFromDropDownsByLocatorAndText(By byLocator, string textToSelect)
    {
      var webElements = AtafWebDriverFindElement.GetWebElements(null, null, byLocator,
        AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC);
      foreach (var webElement in webElements)
      {
        var select = new SelectElement(webElement);
        select.SelectByText(textToSelect);
      }
    }

    public String GetSelectedTextFromDropDownByLocator(By byLocator)
    {
      var webElement = AtafWebDriverFindElement.GetWebElement(null, null, byLocator);
      var select = new SelectElement(webElement);
      return select.SelectedOption.Text;
    }

    public void HoverByLocator(By byLocator)
    {
      Actions action = new Actions(AtafWebDriverFindElement.AtafDriver);
      action.MoveToElement(AtafWebDriverFindElement.GetWebElement(null, null, byLocator)).Perform();
    }

    public void AcceptPopupAlert()
    {
      AtafWebDriverFindElement.AtafDriver.SwitchTo().Alert().Accept();
    }

    public string GetTextForElementByLocator(By byLocator)
    {
     return AtafWebDriverFindElement.GetWebElement(null, null, byLocator).Text;
    }


    public string GetValueForElementByLocator(By byLocator)
    {
      return AtafWebDriverFindElement.GetWebElement(null, null, byLocator).GetAttribute("value");
    }

    public string GetAttributeValueForElementByLocator(By byLocator, string typeOfAttribute)
    {
      return AtafWebDriverFindElement.GetWebElement(null, null, byLocator).GetAttribute(typeOfAttribute);
    }


    public string GetIdForElementByLocator(By byLocator)
    {
      return AtafWebDriverFindElement.GetWebElement(null, null, byLocator).GetAttribute("id");
    }

    public string GetSourceOfPage()
    {
      AtafCommonHelpers.WaitWhileFalseAndMaxTime(()=> !string.IsNullOrEmpty(AtafWebDriverFindElement.AtafDriver.PageSource));
      return AtafWebDriverFindElement.AtafDriver.PageSource;
    }

    public string GetSourceOfPageWithoutWhiteSpaces()
    {

      string pattern = @"<(.|\n)*?>";
      string sourceOfPageWithoutHtmlTags = Regex
        .Replace(GetSourceOfPage(), pattern, string.Empty).Replace("&nbsp;", "");
      return AtafCommonHelpers.RemoveWhitespace(sourceOfPageWithoutHtmlTags).ToLower();
    }

    public string GetColorForElementByLocator(By byLocator)
    {
      return AtafWebDriverFindElement.GetWebElement(null, null, byLocator).GetCssValue("color"); 
    }
    public void ExecuteJavaScript(string javaScriptToExecute)
    {
      AtafWebDriverFactory.AtafDriver.ExecuteScript(javaScriptToExecute);
    }

    public static T ExecuteJavaScript<T>(string scripts)
    {
      IJavaScriptExecutor js = (IJavaScriptExecutor)AtafWebDriverFactory.AtafDriver;
      return (T)js.ExecuteScript(scripts);
    }


    public IWebElement GetElementByLocator(By byLocator)
    {
      return AtafWebDriverFindElement.GetWebElement(null, null, byLocator);
    }

    public void ClickElementUsingJavaScriptAndLocator(By byLocator ,string javaScriptToExecute= "arguments[0].click();")
    {
      AtafWebDriverFactory.AtafDriver.ExecuteScript(javaScriptToExecute, AtafWebDriverFindElement.GetWebElement(null, null, byLocator));
    }

  }
}
