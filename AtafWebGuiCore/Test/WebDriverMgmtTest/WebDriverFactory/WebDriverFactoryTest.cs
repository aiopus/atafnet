﻿using NUnit.Framework;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Text;
using AtafCommonCore.AtafAppConfigSettingMgmt;
using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafNUnitMgmt;
using AtafWebGuiCore.WebDriver;
using OpenQA.Selenium;
using static AtafWebGuiCore.WebDriver.AtafWebDriverFindElement;
using AtafCommonCore.Test;
using AtafWebGuiCore.Common;

namespace AtafWebGuiCore.Test.WebDriverMgmtTest.WebDriverFactory
{
  [AtafNUnitTestManager]
  class WebDriverFactoryTest : AtafInstantiateDriverClass
  {
    [Test]
    public void WebDriverStart()
    {
      InstantiateWebDriver();
      AtafPropertySettingHandler.GetAllSolutionAppConfigSettings();
        AtafPropertySettingHandler.AssignPropertySettingsToObject();
      AtafWebDriverFactory.StartWebDriverApplication(AtafPropertySettingHandler.ATAF_WEB_GUI_DRIVER_TO_RUN);
      AtafWebDriverFactory.GoToSite("http://www.google.com");
      atafWebDriverOperateOnElement.InsertInToFieldByLocator(By.Name("q"), "Selenium");
      atafWebDriverOperateOnElement.ClickElementByLocator(By.Name("btnK"));
    }

    [TearDown]
    public void TearDownCase()
    {
     AtafLog.TestCaseEnd();
    }
  }
}
