﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;

namespace AtafRestApiCore.AtafRestApiMgmt.AtafRestSharpWrapper
{
  public class AtafRestSharpClientFactory
  {
    protected static RestClient Client { get; private set; }

    public static RestClient CreateRestClient(String baseUrl, String basePath)
    {
      Client = new RestClient(baseUrl +"/"+ basePath);
      Client.Encoding=Encoding.UTF8;
      return Client;
    }

    public static RestClient CreateRestClientSimpleAuthenticator(
      string baseUrl,
      string basePath,
      string usernameKey,
      string username,
      string passwordKey,
      string password)
    {
      Client = new RestClient(baseUrl + "/" + basePath);
      Client.Encoding = Encoding.UTF8;
      Client.Authenticator = new SimpleAuthenticator(usernameKey,username,passwordKey, password);
      return Client;
    }

    public static RestClient CreateRestClientHttpBasicAuthenticator(
      string baseUrl,
      string basePath,
      string username,
      string password)
    {
      Client = new RestClient(baseUrl + "/" + basePath);
      Client.Encoding = Encoding.UTF8;
      Client.Authenticator = new HttpBasicAuthenticator(username, password);
      return Client;
    }

    public static RestClient CreateRestClientNtlmAuthenticator(
      string baseUrl,
      string basePath,
      string username,
      string password)
    {
      Client = new RestClient(baseUrl + "/" + basePath);
      Client.Encoding = Encoding.UTF8;
      Client.Authenticator = new NtlmAuthenticator( username,  password);
      return Client;
    }

    public static RestClient CreateRestClientSimpleAuthenticator(
      string baseUrl,
      string basePath,
      string accessToken,
      string tokenType)
    {
      Client = new RestClient(baseUrl + "/" + basePath);
      Client.Encoding = Encoding.UTF8;
      Client.Authenticator = new OAuth2AuthorizationRequestHeaderAuthenticator(accessToken, tokenType);
      return Client;
    }

  }
}
