﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using Newtonsoft.Json;
using RestSharp;

namespace AtafRestApiCore.AtafRestApiMgmt.AtafRestSharpWrapper
{
  public class AtafRestSharpLog : AtafRestSharpClientFactory
  {
    static string INDENT_STRING = "    ";

    public static void LogRestRequest(RestRequest restRequest)
    {
      List<string> restRequestListToLog = new List<string>();

      restRequestListToLog.Add("METHOD: " + JsonConvert.SerializeObject(restRequest.Method.ToString()));
      restRequestListToLog.Add("ENDPOINT URL: " +
                               JsonConvert.SerializeObject(Client.BaseUrl.AbsoluteUri + restRequest.Resource));
      foreach (var param in restRequest.Parameters)
      {
        string s = JsonConvert.SerializeObject(param.Value);
        restRequestListToLog.Add(param.Type.ToString().ToUpper() + ":\n" + INDENT_STRING + FormatFromStringToJson(s));
      }
      restRequestListToLog.Add("AUTHENTICATOR: " + JsonConvert.SerializeObject(Client.Authenticator));
      restRequestListToLog.Add("ENCODING: " + JsonConvert.SerializeObject(Client.Encoding.EncodingName));
      restRequestListToLog.Add("REQUEST FORMAT: " + JsonConvert.SerializeObject(restRequest.RequestFormat.ToString()));
      

      string restRequestListToLogAsString = "\n################################## REQUEST ###############################\n";

      foreach (string restResponseToLog in restRequestListToLog)
      {
        restRequestListToLogAsString = restRequestListToLogAsString + restResponseToLog + "\n";
      }

      AtafLog.Info(restRequestListToLogAsString);
    }

    public static void LogRestResponse<T>(IRestResponse<T> restResponse)
    {
      List<string> restResponseListToLog = new List<string>();
      restResponseListToLog.Add("\nSTATUS CODE:" + JsonConvert.SerializeObject(restResponse.StatusCode));
      if (!string.IsNullOrEmpty(restResponse.ErrorMessage))
      {
        restResponseListToLog.Add("ERROR MESSAGE: " +
                                  JsonConvert.SerializeObject(restResponse.ErrorMessage, Formatting.None));
        restResponseListToLog.Add("ERROR STACKTRACE: " +
                                  JsonConvert.SerializeObject(restResponse.ErrorException.StackTrace, Formatting.None));
      }
        
      restResponseListToLog.Add("RESPONSE BODY: \n" + FormatFromStringToJson(restResponse.Content));
      restResponseListToLog.Add("RESPONSE URL: " + JsonConvert.SerializeObject(restResponse.ResponseUri));
      restResponseListToLog.Add("##### HEADERS START #####");
      foreach (var header in restResponse.Headers)
      {
        restResponseListToLog.Add(INDENT_STRING + header.Name.ToUpper() + ": " + JsonConvert.SerializeObject(header.Value));
      }
      restResponseListToLog.Add("##### HEADERS END #####");

      if (restResponse.Cookies.Count > 1)
      {
        restResponseListToLog.Add("##### COOKIES START #####");
        foreach (var cookie in restResponse.Cookies)
        {
          restResponseListToLog.Add(INDENT_STRING + cookie.Name.ToUpper() + ": " + JsonConvert.SerializeObject(cookie.Value));
        }
        restResponseListToLog.Add("##### COOKIES END #####");
      }

      string restResponseListToLogAsString = "\n ################################## RESPONSE ###############################";

      foreach (string restResponseToLog in restResponseListToLog)
      {
        restResponseListToLogAsString = restResponseListToLogAsString + restResponseToLog + "\n";
      }

      AtafLog.Info(restResponseListToLogAsString);
    }

    public static string FormatFromStringToJson(string str)
    {
      var indent = 0;
      var quoted = false;
      var sb = new StringBuilder();
      try
      {
        for (var i = 0; i < str.Length; i++)
        {
          var ch = str[i];
          switch (ch)
          {
            case '{':
            case '[':
              sb.Append(ch);
              if (!quoted)
              {
                sb.AppendLine();
                Enumerable.Range(0, ++indent).ForEach(item => sb.Append(INDENT_STRING));
              }

              break;
            case '}':
            case ']':
              if (!quoted)
              {
                sb.AppendLine();
                Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
              }

              sb.Append(ch);
              break;
            case '"':
              sb.Append(ch);
              bool escaped = false;
              var index = i;
              while (index > 0 && str[--index] == '\\')
                escaped = !escaped;
              if (!escaped)
                quoted = !quoted;
              break;
            case ',':
              sb.Append(ch);
              if (!quoted)
              {
                sb.AppendLine();
                Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
              }

              break;
            case ':':
              sb.Append(ch);
              if (!quoted)
                sb.Append(" ");
              break;
            default:
              sb.Append(ch);
              break;
          }
        }

        return sb.ToString();
      }
      catch (Exception e)
      {
        AtafLog.Warning(e.Message);
        return str;
      }

    }
  }

  static class Extensions
  {
    public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
    {
      foreach (var i in ie)
      {
        action(i);
      }
    }
  }
}
