﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace AtafRestApiCore.AtafRestApiMgmt.AtafRestSharpWrapper
{
  public class AtafRestSharpHeaderAndCookieFactory
  {

    public static void SetHeaderAndCookies(RestRequest restRequest, string headerDoubleColonCommaSepList, string cookieDoubleColonCommaSepList)
    {
      if (!string.IsNullOrEmpty(headerDoubleColonCommaSepList))
      {
        string[] headerList = null;
        if (headerDoubleColonCommaSepList.Contains("::"))
        {
          headerList = headerDoubleColonCommaSepList.Split(Convert.ToChar("::"));
        }
        else
        {
          headerList = new string[] { headerDoubleColonCommaSepList };
        }

        foreach (var header in headerList)
        {
          string headerName = header.Substring(0, header.IndexOf(","));
          string headerValue = header.Substring(header.IndexOf(",") + 1);
          restRequest.AddHeader(headerName, headerValue);
        }
      }

      if (!string.IsNullOrEmpty(cookieDoubleColonCommaSepList))
      {
        string[] cookieList = null;
        if (cookieDoubleColonCommaSepList.Contains("::"))
        {
          cookieList = cookieDoubleColonCommaSepList.Split(Convert.ToChar("::"));
        }
        else
        {
          cookieList = new string[] { cookieDoubleColonCommaSepList };
        }
        foreach (var cookie in cookieList)
        {
          string cookieName = cookie.Substring(0, cookie.IndexOf(","));
          string cookieValue = cookie.Substring(cookie.IndexOf(",") + 1);
          restRequest.AddCookie(cookieName, cookieValue);
        }
      }
    }
  }
}
