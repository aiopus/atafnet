﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using AtafRestApiCore.AtafRestApiMgmt.RequestBodyBuilder.Employee.RequestBody;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Extensions;
using ContentType = RestSharp.Serialization.ContentType;

namespace AtafRestApiCore.AtafRestApiMgmt.AtafRestSharpWrapper
{
  public class AtafRestApiRequestFactory : AtafRestSharpClientFactory
  {
    public static async Task<IRestResponse<T>> Get<T>(string pathToEndPoint, string headerDoubleColonCommaSepList = null,
      string cookieDoubleColonCommaSepList = null) where T : class, new()
    {
      AtafLog.Debug(pathToEndPoint, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);
      return await RequestAndLog<T>(pathToEndPoint, Method.GET, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);
    }
    public static Stream GetFile(string pathToEndPoint, string headerDoubleColonCommaSepList = null, string cookieDoubleColonCommaSepList = null)
    {
      AtafLog.Debug(pathToEndPoint, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);
      var restRequest = RestRequestModified(new RestRequest(pathToEndPoint, Method.GET),
        headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList, null);
      var response=Get<object>(pathToEndPoint, headerDoubleColonCommaSepList = null, cookieDoubleColonCommaSepList = null).Result;
      return new MemoryStream(response.RawBytes);
    }

    public static async  Task<IRestResponse<T>> Post<T>(string pathToEndPoint, Object postRequestBody, string headerDoubleColonCommaSepList = null,
      string cookieDoubleColonCommaSepList = null) where T : class, new()
    {
      AtafLog.Debug(pathToEndPoint, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);
      return await RequestAndLog<T>(pathToEndPoint, Method.POST, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList,postRequestBody);

    }

    public static async  Task<IRestResponse<T>> Delete<T>(string pathToEndPoint, string headerDoubleColonCommaSepList = null,
      string cookieDoubleColonCommaSepList = null) where T : class, new()
    {
      AtafLog.Debug(pathToEndPoint, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);
      return await RequestAndLog<T>(pathToEndPoint, Method.DELETE, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);

    }

    private static async Task<IRestResponse<T>> RequestAndLog<T>(string pathToEndPoint, Method requestMethod,
      string headerDoubleColonCommaSepList = null,
      string cookieDoubleColonCommaSepList = null, Object requestBody=null) where T : new()
    {
      AtafLog.Debug(pathToEndPoint, requestMethod.ToString(), headerDoubleColonCommaSepList,
        cookieDoubleColonCommaSepList);

      var restRequest = RestRequestModified(new RestRequest(pathToEndPoint, requestMethod),
        headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList, requestBody);

      AtafRestSharpLog.LogRestRequest(restRequest);

      IRestResponse<T> restResponse = await ExecuteAsyncRequestTask<T>(Client, restRequest);

      AtafRestSharpLog.LogRestResponse(restResponse);
      if (restResponse.StatusCode == HttpStatusCode.InternalServerError)
      {
        throw new Exception(restResponse.ErrorMessage);
      }
      return restResponse;
    }
    private static RestRequest RestRequestModified(RestRequest restRequest, string headerDoubleColonCommaSepList = null,
      string cookieDoubleColonCommaSepList = null, object requestBody = null)
    {
      AtafRestSharpHeaderAndCookieFactory.SetHeaderAndCookies(restRequest, headerDoubleColonCommaSepList, cookieDoubleColonCommaSepList);
      restRequest.RequestFormat = DataFormat.Json;
    restRequest.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
        restRequest.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
        restRequest.JsonSerializer.ContentType = ContentType.Json;
        restRequest.AddBody(requestBody);
      return restRequest;
    }
    private static async Task<IRestResponse<T>> ExecuteAsyncRequestTask<T>(RestClient restClient, RestRequest request) where T : new()
    {
      IRestResponse<T> response = await restClient.ExecuteTaskAsync<T>(request);
      //response.ContentType= "application/json";
      return response;
    }
    
  }
}
