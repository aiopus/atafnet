﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtafRestApiCore.AtafRestApiMgmt.RequestBodyBuilder.Employee.RequestBody
{

  public class PostCreateRequestBody
  {
    public string employee_name { get; set; }
    public string employee_salary { get; set; }
    public string employee_age { get; set; }
  }
}
