﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafNUnitMgmt;
using AtafRestApiCore.AtafRestApiMgmt.AtafRestSharpWrapper;
using AtafRestApiCore.AtafRestApiMgmt.RequestBodyBuilder.Employee.RequestBody;
using AtafRestApiCore.AtafRestApiMgmt.RequestBodyBuilder.Employee.Response;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;

namespace AtafRestApiCore.Test
{
  [AtafNUnitTestManager]
  class RestCharpTest
  {
    [Test]
    public void GetTest()
    {
      AtafRestSharpClientFactory.CreateRestClient("http://dummy.restapiexample.com", "api/v1/");

      var employees = AtafRestApiRequestFactory.Get<List<Employee>>( "employees");
      var em=JsonConvert.DeserializeObject<Employee>(employees.Result.Content);
      Assert.NotNull(em);
    } 
    
    [Test]
    public void PostTest()
    {
      AtafRestSharpClientFactory.CreateRestClient("http://dummy.restapiexample.com", "api/v1/");

      PostCreateRequestBody postCreateRequestBody= new PostCreateRequestBody();
      postCreateRequestBody.employee_age = "31";
      postCreateRequestBody.employee_name="Georgehgfkhgfkhgfkhgf Kjgfkghf";
      postCreateRequestBody.employee_salary = "1000";

      Employee employee= AtafRestApiRequestFactory.Post<Employee>( "create", postCreateRequestBody).Result.Data;
      Assert.NotNull(employee);
    }

    [Test]
    public void PutTest()
    {
      AtafRestSharpClientFactory.CreateRestClient("http://dummy.restapiexample.com", "api/v1/");

      PostCreateRequestBody postCreateRequestBody = new PostCreateRequestBody();
      postCreateRequestBody.employee_age = "31";
      postCreateRequestBody.employee_name = "Georgehgfkhgfkhgfkhgf Kjgfkghf";
      postCreateRequestBody.employee_salary = "1000";

      Employee employee = AtafRestApiRequestFactory.Post<Employee>("create", postCreateRequestBody).Result.Data;
      Assert.NotNull(employee);
    }
  }
}
