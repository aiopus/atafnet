﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtafCommonCore.AtafUtilityMgmt
{
  public class AtafXPathHelper
  {
    public static string GetXPathByTagAndTextContains(string tag, string text)
    {
      return "//" + tag + "[contains(text(),'" + text + "')]";
    }

    public static string GetXPathByTagAndTextContainsAndIndex(string tag, string text, int indexOfElementToSelectFromXpathResultList)
    {
      return "(//" + tag + "[contains(text(),'" + text + "')])["+indexOfElementToSelectFromXpathResultList+"]";
    }
    public static string GetXPathByTagAndClass(string tag, string className)
    {
      return "//" + tag + "[contains(@class, '" + className + "')]";
    }
    public static string GetXPathByTagAndClassAndIndex(string tag, string className, int indexOfElementToSelectFromXpathResultList)
    {
      return "(//" + tag + "[contains(@class, '" + className + "')])[" + indexOfElementToSelectFromXpathResultList + "]";
    }
   
    public static string GetXPathByTagAndValue(string tag, string value)
    {
      return "//" + tag + "[contains(@value, '" + value + "')]";
    }

    public static string GetXPathByTagAndTypeOfAttributeAndAttributeValue(string tag, string typeOfAttribute, string attributeValue)
    {
      return "//" + tag + "[contains(@"+ typeOfAttribute +", '" + attributeValue + "')]";
    }

    public static string GetXPathByTagAndClassAndTextContains(string tag, string className, string text)
    {
      return "//" + tag + "[contains(@class, '"+ className+"') and contains(text(),'" + text + "')]";
    }
    public static string GetXPathByTagAndClassAndTextContainsAndIndex(string tag, string className, string text, int indexOfElementToSelectFromXpathResultList)
    {
      return "(//" + tag + "[contains(@class, '" + className + "') and contains(text(),'" + text + "')])[" + indexOfElementToSelectFromXpathResultList + "]";
    }
    public static string GetXPathByTagAndIdContains(string tag, string idContains)
    {
      return "//" + tag + "[@id[contains(., '" + idContains + "')]]";
    }
  
  }
}
