﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using AtafCommonCore.AtafLogMgmt;

namespace AtafCommonCore.AtafUtilityMgmt
{
  public class AtafExcelHelper
  {

    public static bool IsThereDiffBetweenTwoExcelSheets(string absolutPathExcelFile1, string sheetName1,
      string absolutPathExcelFile2, string sheetName2, int columnStartIndex = 0, int columnEndIndex = -1,
      int rawStartIndex = 0, int rawEndIndex = -1)
    {
      using (DataTable sheet1 = CreateDatatableFromExcelSheet(absolutPathExcelFile1, sheetName1),
        sheet2 = CreateDatatableFromExcelSheet(absolutPathExcelFile2, sheetName2))
      {
        DataTable table = new DataTable();
        DataColumnCollection columnsSheet1ToTreat = sheet1.Columns;
        DataColumnCollection columnsSheet2ToTreat = sheet2.Columns;

        table.Columns.Add("FirstHeading", typeof(string));
        table.Columns.Add("SecondHeading", typeof(string));
        table.Columns.Add("ValueinSheet1", typeof(string));
        table.Columns.Add("ValueinSheet2", typeof(string));
        string test1 = null;
        string test2 = null;
        if (columnEndIndex == -1)
        {
          columnEndIndex = sheet1.Columns.Count;
        }

        if (rawEndIndex == -1)
        {
          rawEndIndex = sheet1.Rows.Count;
        }

        for (int ci1 = columnStartIndex; ci1 < columnEndIndex; ci1++)
        {
          string a = columnsSheet1ToTreat[ci1].ColumnName;
          for (int ci2 = columnStartIndex; ci2 < columnEndIndex; ci2++)
          {
            string b = columnsSheet2ToTreat[ci2].ColumnName;
            if (a == b)
            {
              List<object> lst11 = (from d in sheet1.AsEnumerable() select d.Field<object>(a)).ToList();
              List<object> lst1 = lst11.GetRange(rawStartIndex, rawEndIndex - rawStartIndex);
              List<object> lst22 = (from d in sheet2.AsEnumerable() select d.Field<object>(a)).ToList();
              List<object> lst2 = lst22.GetRange(rawStartIndex, rawEndIndex - rawStartIndex);

              for (int i = 0; i < lst1.Count; i++)
              {
                for (int j = i; j < lst2.Count; j++)
                {
                  if ((i == j) && (!lst1.SequenceEqual(lst2)))
                  {

                    AtafLog.Debug("lst1[i].ToString():" + lst1[i]);
                    AtafLog.Debug("lst2[j].ToString(): " + lst2[j]);

                    if (lst1[i] != null && lst2[i] != null && lst1[i].ToString() != lst2[j].ToString())
                    {
                      test1 = lst1[i].ToString();
                      test2 = lst2[i].ToString();
                      string name1 = sheet1.Rows[0][a].ToString();
                      string name2 = sheet2.Rows[0][a].ToString();
                      if (name1 == name2)
                      {
                        table.Rows.Add(a, name1, test1, test2);
                      }
                    }
                  }
                }
              }
            }
          }

          DataView dataView = new DataView(table);
          table = dataView.ToTable(true);
          if (table.Rows.Count > 0)
          {
            var lines = new List<string>();

            string[] columnNames = table.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();

            var header = string.Join(",", columnNames);
            lines.Add(header);

            var valueLines = table.AsEnumerable().Select(row => string.Join(",", row.ItemArray));
            lines.AddRange(valueLines);
            string stringToPrint = "\n" + string.Join("\n", lines);
            AtafLog.Info(stringToPrint);
            return true;
          }
        }

        return false;
      }
    }

    public static DataTable CreateDatatableFromExcelSheet(string absolutPathToExcelFile, string sheetName)
    {
      string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + absolutPathToExcelFile + ";" +
                                "Extended Properties='Excel 12.0 Xml;HDR=YES;IMEX=1;MAXSCANROWS=0'";

      DataTable dt = new DataTable();
      using (OleDbConnection conn = new OleDbConnection(connectionString))
      {
        using (OleDbCommand comm = new OleDbCommand())
        {
          comm.CommandText = "Select * from [" + sheetName + "$]";
          comm.Connection = conn;
          using (OleDbDataAdapter da = new OleDbDataAdapter())
          {
            da.SelectCommand = comm;
            da.Fill(dt);
            return dt;
          }
        }

      }
    }


  }
}