﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;

namespace AtafCommonCore.AtafUtilityMgmt
{
  public class AtafFileHelper
  {

    public static string StreamToString(MemoryStream memoryStream)
    {
      StreamReader reader = new StreamReader(memoryStream, Encoding.UTF8, true);
      return reader.ReadToEnd();
    }

    public static string StreamToString(Stream stream)
    {
      StreamReader reader = new StreamReader(stream, Encoding.UTF8, false);
      return reader.ReadToEnd();
    }


    public static void DeleteOldFilesInDir(string absolutPathToDir, int numberOfFilesToKeep)
    {
      AtafLog.Info(absolutPathToDir, numberOfFilesToKeep.ToString());
      ((IEnumerable<FileInfo>)new DirectoryInfo(absolutPathToDir).GetFiles()).OrderByDescending<FileInfo, DateTime>((Func<FileInfo, DateTime>)(f => f.LastWriteTime)).Skip<FileInfo>(10).ToList<FileInfo>().ForEach((Action<FileInfo>)(f => f.Delete()));
    }

    public static Stream GetResourceFileAsStream(string fileName)
    {
      AtafLog.Info(fileName);
      Assembly executingAssembly = Assembly.GetExecutingAssembly();
      string name = ((IEnumerable<string>)executingAssembly.GetManifestResourceNames()).Single<string>((Func<string, bool>)(str => str.ToLower().EndsWith(fileName.ToLower())));
      return executingAssembly.GetManifestResourceStream(name);
    }

    public static Stream GetResourceFileAsStream(Assembly assembly, string fileName)
    {
      AtafLog.Info(assembly.FullName, fileName);
      string name = ((IEnumerable<string>)assembly.GetManifestResourceNames()).Single<string>((Func<string, bool>)(str => str.ToLower().EndsWith(fileName.ToLower())));
      return assembly.GetManifestResourceStream(name);
    }

    public static FileInfo GetReourceStreamAsFile(Stream stream, string destFilePath)
    {
      AtafLog.Info(stream.Length.ToString(), destFilePath);
      FileStream fileStream = new FileStream(destFilePath, FileMode.Create, FileAccess.Write);
      stream.CopyTo((Stream)fileStream);
      FileInfo fileInfo = new FileInfo(fileStream.Name);
      fileStream.Dispose();
      fileStream.Close();
      return fileInfo;
    }

    /// <summary>
    /// Create a file and save the text in the file
    /// </summary>
    /// <param name="textToSaveToFile"></param>
    /// <param name="absolutePathDirToSavedFile"></param>
    /// <param name="fileName"></param>
    public static void SaveTextIntoFile(
      string textToSaveToFile,
      string absolutePathDirToSavedFile,
      string fileName)
    {
      
      AtafLog.Info(textToSaveToFile, absolutePathDirToSavedFile, fileName);
      Directory.CreateDirectory(absolutePathDirToSavedFile);
      File.WriteAllText(absolutePathDirToSavedFile + "/" + fileName, textToSaveToFile, Encoding.UTF8);
    }


    public static string GetContentForLastUpdatedFileAsString(string absolutePathDir, string fileName)
    {
      AtafLog.Info(absolutePathDir, fileName);
      var baselineDirInfo = new DirectoryInfo(absolutePathDir);
      var latestFile = baselineDirInfo.GetFiles(fileName).OrderByDescending(f => f.LastWriteTime).First();
      return @File.ReadAllText(absolutePathDir + "/" + latestFile, Encoding.UTF8);
    }

    public static string GetContentForFileAsString(string absolutePathDirIncludingfileName)
    {
      AtafLog.Info( absolutePathDirIncludingfileName);
      return @File.ReadAllText(absolutePathDirIncludingfileName, Encoding.UTF8);
    }


    public static void CreateFileFromStream(string absolutePathDir, string fileName, Stream inputStream)
    {
      AtafLog.Info(absolutePathDir, fileName, inputStream.Length.ToString());
      using (inputStream)
      {
        Directory.CreateDirectory(absolutePathDir);
        FileStream fileStream = File.Create(absolutePathDir + @"/" + fileName);
        inputStream.CopyTo(fileStream);
        fileStream.Close();
        fileStream.Dispose();
        inputStream.Close();
        inputStream.Dispose();
      }
    }
  }
}
