﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AtafCommonCore.AtafAppConfigSettingMgmt;
using AtafCommonCore.AtafLogMgmt;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Web.Mvc;

namespace AtafCommonCore.AtafUtilityMgmt
{
  public class AtafCommonHelpers
  {

    public static string GetOutPutDirectory()
    {
      return Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
    }

    public static string GetProjectDirectory()
    {
      AtafLog.Debug();
      var outPutDirectory = GetOutPutDirectory();
      int startIndex = outPutDirectory.IndexOf("\\") + 1;
      int binIndex = outPutDirectory.IndexOf("bin");
      return outPutDirectory.Substring(startIndex, binIndex - startIndex);
    }

    public static string GetSolutionDirectory()
    {
      AtafLog.Debug();
      string projectDir = GetProjectDirectory().Substring(0, GetProjectDirectory().Length - 1);
      return projectDir.Substring(0, projectDir.LastIndexOf("\\"));
    }

    public static bool WaitWhileFalseAndMaxTime(Func<bool> methodToRun)
    {
      var maxTimeToWait = DateTime.Now.ToUniversalTime()
        .AddMilliseconds(AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC);
      while (!methodToRun.Invoke() && DateTime.Now.ToUniversalTime() < maxTimeToWait)
      {
        Thread.Sleep(500);
      }

      if (DateTime.Now.ToUniversalTime() < maxTimeToWait)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public static T WaitWhileFalseAndMaxTime<T>(Func<T> booleanToExecute, Action methodToExecute,
      string milliSecondsToSleepBeforeMethodToExecute = null)
    {
      int milliSecondsToSleep = milliSecondsToSleepBeforeMethodToExecute == null
        ? 500
        : int.Parse(milliSecondsToSleepBeforeMethodToExecute);
      var maxTimeToWait = DateTime.Now.ToUniversalTime()
        .AddMilliseconds(AtafPropertySettingHandler.ATAF_WEB_GUI_MAX_TIME_TO_WAIT_MSEC);
      while (!(bool) booleanToExecute.DynamicInvoke() && DateTime.Now.ToUniversalTime() < maxTimeToWait)
      {
        Thread.Sleep(milliSecondsToSleep);
        try
        {
          methodToExecute.Invoke();
          Thread.Sleep(milliSecondsToSleep);
        }
        catch (Exception e)
        {
          AtafLog.Info(e.Message, e.StackTrace);
        }
      }

      if (!(bool) booleanToExecute.DynamicInvoke())
      {
        throw new Exception("The Execution of booleanToExecute is, " + booleanToExecute.DynamicInvoke().ToString());
      }

      return booleanToExecute.Invoke();
    }

    /// <summary>
    ///  Return a date numberOfDaysAheadFromToday
    /// numberOfDaysAheadFromToday ends up on Saturday to Sunday then returned date will be a Monday
    /// </summary>
    /// <param name="numberOfDaysAheadFromToday"></param>
    /// <param name="dateFormate"></param>
    /// <returns></returns>
    public static string GetDateWeekDaysAheadFromToday(int numberOfDaysAheadFromToday,
      string dateFormate = "yyyy-MM-dd")
    {
      DateTime dateTimenNow = DateTime.Now;
      DateTime dateTimeAhead = dateTimenNow.AddDays(numberOfDaysAheadFromToday);
      if (dateTimeAhead.DayOfWeek == DayOfWeek.Saturday)
      {
        dateTimeAhead = dateTimenNow.AddDays(numberOfDaysAheadFromToday + 2);
      }
      else if (dateTimeAhead.DayOfWeek == DayOfWeek.Sunday)
      {

        dateTimeAhead = dateTimenNow.AddDays(numberOfDaysAheadFromToday + 1);
      }

      return dateTimeAhead.ToString(dateFormate);
    }

    public static void TakeWebDriverScreenShot(RemoteWebDriver driver = null, FlaUI.Core.Application application = null)
    {
      string pathScreenshotFile = null;
      string dateTime = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff");
      string testClassName =
        TestContext.CurrentContext.Test.ClassName.Substring(TestContext.CurrentContext.Test.ClassName.LastIndexOf(".") +
                                                            1);
      string testCaseName =
        TestContext.CurrentContext.Test.MethodName.Substring(
          TestContext.CurrentContext.Test.MethodName.LastIndexOf(".") + 1);
      DirectoryInfo directoryInfo =
        Directory.CreateDirectory(GetProjectDirectory() + "Screenshot/" + testClassName + "/");
      String trunckedTestCaseName = testCaseName;
      if ((directoryInfo.FullName + testCaseName).Length > 201)
      {
        pathScreenshotFile = (directoryInfo.FullName + testCaseName).Substring(0, 200);
        trunckedTestCaseName = pathScreenshotFile.Substring(pathScreenshotFile.LastIndexOf(directoryInfo.FullName) + 1);
      }
      else
      {
        pathScreenshotFile = directoryInfo.FullName + testCaseName;
      }

      pathScreenshotFile = ((directoryInfo.FullName + testCaseName).Length > 201
        ? (directoryInfo.FullName + testCaseName).Substring(0, 200)
        : directoryInfo.FullName + testCaseName) + "_Failed" + dateTime + ".png";


      if (driver != null)
      {
        Screenshot screenshot = ((ITakesScreenshot) driver).GetScreenshot();
        screenshot.SaveAsFile(pathScreenshotFile, ScreenshotImageFormat.Png);
        TestContext.AddTestAttachment(pathScreenshotFile);
      }

      if (application != null)
      {
        var screenshot = FlaUI.Core.Capturing.Capture.Screen();
        screenshot.ToFile(pathScreenshotFile);
      }

      if (String.IsNullOrEmpty(AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH) ||
          AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH.Contains("file"))
      {
        AtafLog.Info("file:///" + pathScreenshotFile);
      }
      else
      {
        AtafLog.Info(AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH +
                     pathScreenshotFile.Substring(pathScreenshotFile.IndexOf(":") + 1));

        if (!string.IsNullOrEmpty(AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_PERSISTED_SCREENSHOT_BASE_PATH))
        {
          int indexTestClassName = pathScreenshotFile.IndexOf(testClassName) + testClassName.Length;
          string pathScreenshotPersistedFile = AtafPropertySettingHandler.ATAF_WEB_DESKTOP_GUI_SCREENSHOT_BASE_PATH +
                                               AtafPropertySettingHandler
                                                 .ATAF_WEB_DESKTOP_GUI_PERSISTED_SCREENSHOT_BASE_PATH.Substring(
                                                   pathScreenshotFile.IndexOf(":") + 1) + "/" +
                                               testClassName +
                                               pathScreenshotFile.Substring(indexTestClassName);

          AtafLog.Info(pathScreenshotPersistedFile);
        }
      }
    }

    public static string RemoveWhitespace(string stringToRemoveWhiteSpaceFor)
    {
      return new string(stringToRemoveWhiteSpaceFor
        .ToCharArray()
        .Where(c => !Char.IsWhiteSpace(c))
        .ToArray());
    }

    public static bool CompareTwoStrings(string text1, string text2, List<string> diffsToIgnore = null)
    {
      string diffsToPrint = "";
      List<string> diff1;
      List<string> diff2;
      IEnumerable<string> set1 = text1.Split(' ').Distinct();
      IEnumerable<string> set2 = text2.Split(' ').Distinct();

      diff1 = set2.Except(set1).ToList();

      diff2 = set1.Except(set2).ToList();
      if (diffsToIgnore != null)
      {
        foreach (var diffToIgnore in diffsToIgnore)
        {
          diff1 = diff1.FindAll(d => !d.Contains(diffToIgnore)).ToList();
          diff2 = diff2.FindAll(d => !d.Contains(diffToIgnore)).ToList();
        }
      }

      if (diff1.Count > 0)
      {
        for (int i = 0; i < diff1.Count; i++)
        {
          diffsToPrint = $"{diffsToPrint} text1: {diff1[i]}, text2: {diff2[i]} \n";
        }

        AtafLog.Info(diffsToPrint);
        return true;
      }

      return false;
    }

    public static string GetCamelCaseJsonFromObject(object obj)
    {
      return new ContentResult
      {
        ContentType = "application / json",
        Content = JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }),
        ContentEncoding = Encoding.UTF8,
      }.Content;
    }
  }
}