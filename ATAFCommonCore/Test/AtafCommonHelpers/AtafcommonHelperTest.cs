﻿using System.Collections.Generic;
using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafNUnitMgmt;
using NUnit.Framework;

namespace AtafCommonCore.Test.AtafCommonHelpers
{
  [AtafNUnitTestManager]
  public class AtafcommonHelperTest
  {
    [OneTimeSetUp]
    public void OneTimeSetUpForClass()
    {
      AtafLog.TestClassStart();
    }

    [SetUp]
    public void SetUpTestCase()
    {
      AtafLog.TestCaseStart();

    }

    [Test]
    public void Given_2_strings_with_no_diff_when_Compared_Then_diff_false()
    {
      Assert.False(AtafUtilityMgmt.AtafCommonHelpers.CompareTwoStrings
      (
        "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><document name=\"test\" date=\"2020-02-29\" level=\"test\" source=\"5376\"><recs_type_i930 f001=\"test\" c200=\"Bal0_014335\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"1\" a017=\"0\" a018=\"1\" a020=\"0\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"2\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014336\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"5\" a017=\"0\" a018=\"1\" a020=\"1\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"2\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014337\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"1\" a017=\"0\" a018=\"1\" a020=\"0\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"4\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014338\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"5\" a017=\"0\" a018=\"1\" a020=\"1\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"4\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014339\" j301=\"S0301\" c013=\"1300\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"\" a017=\"\" a018=\"1\" a020=\"0\" A079=\"0\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"\" />",
        "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><document name=\"test\" date=\"2020-02-29\" level=\"test\" source=\"5376\"><recs_type_i930 f001=\"test\" c200=\"Bal0_011111\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"1\" a017=\"0\" a018=\"1\" a020=\"0\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"2\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014336\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"5\" a017=\"0\" a018=\"1\" a020=\"1\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"2\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014337\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"1\" a017=\"0\" a018=\"1\" a020=\"0\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"4\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014338\" j301=\"S0301\" c013=\"1050\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"5\" a017=\"0\" a018=\"1\" a020=\"1\" A079=\"\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"4\" /><recs_type_i930 f001=\"test\" c200=\"Bal0_014339\" j301=\"S0301\" c013=\"1300\" e100=\"0\" x100=\"100001\" c150=\"0000\" a013_fp=\"\" a017=\"\" a018=\"1\" a020=\"0\" A079=\"0\" c007=\"SEK\" c254=\"1\" a254=\"0\" a108=\"\" />",
        new List<string> {"_"}));
    }
  }
}
