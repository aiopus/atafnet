﻿using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafNUnitMgmt;
using AtafCommonCore.AtafUtilityMgmt;
using NUnit.Framework;

namespace AtafCommonCore.Test.AtafExcelHelper
{
  [AtafNUnitTestManager]
  public class AtafExcelHelperTest
  {
    [OneTimeSetUp]
    public void OneTimeSetUpForClass()
    {
      AtafLog.TestClassStart();
    }

    [SetUp]
    public void SetUpTestCase()
    {
      AtafLog.TestCaseStart();

    }

    [Test]
    public void Given_Log4N_Configured_When_Test_Run_Then_Info_Log_Is_Logged()
    {
      Assert.False(AtafCommonCore.AtafUtilityMgmt.AtafExcelHelper.IsThereDiffBetweenTwoExcelSheets(@"C:\Temp2\testExcelFile.xlsx", "Dashboard", @"C:\Temp2\testExcelFile.xlsx", "Dashboard",5,35,2,33)); //ExcelConnection();
    }
  }
}
