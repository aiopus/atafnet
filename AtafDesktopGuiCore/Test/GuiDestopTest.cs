﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using AtafCommonCore.AtafLogMgmt;
using AtafCommonCore.AtafNUnitMgmt;
using AtafDesktopGuiCore.Common;
using AtafDesktopGuiCore.FlaUiDesktopMgmt;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;
using FlaUI.UIA3;
using NUnit.Framework;
using AutomationElement = FlaUI.Core.AutomationElements.AutomationElement;
using PropertyCondition = FlaUI.Core.Conditions.PropertyCondition;

namespace AtafDesktopGuiCore.Test
{
  [AtafNUnitTestManager]
  public class GuiDestopTest : AtafInstantiateDesktopClass
  {

    [Test]
    public void NotePade()
    {
      InstantiateDesktop(new UIA3PropertyLibrary(), new UIA3Automation());
      AtafDesktopWindowFactory.AtafApplication = AtafDesktopApplicationFactory.LaunchApplication("notepad.exe");
      Window window = AtafDesktopWindowFactory.GetLaunchedApplicationMainWindow()[0];
      AtafDesktopOperateOnElement.InsertInToTextBoxByPropertyCondition(window, AtafConditionFactory.ByAutomationId("15"), "Hello Hello");
      AtafDesktopApplicationFactory.CloseApplication();
    }

    [TearDown]
    public void TearDownCase()
    {
      AtafLog.TestCaseEnd();
    }
  }
}