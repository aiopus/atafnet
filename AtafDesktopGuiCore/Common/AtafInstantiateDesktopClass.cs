﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using AtafDesktopGuiCore.FlaUiDesktopMgmt;
using FlaUI.Core;
using FlaUI.Core.Conditions;

namespace AtafDesktopGuiCore.Common
{
  public class AtafInstantiateDesktopClass
  {
    public static AtafDesktopApplicationFactory AtafDesktopApplicationFactory;
    public static AtafDesktopWindowFactory AtafDesktopWindowFactory;
    public static AtafDesktopGetElementFactory AtafDesktopGetElementFactory;
    public static AtafDesktopOperateOnElement AtafDesktopOperateOnElement;
    public static ConditionFactory AtafConditionFactory;

    public static void InstantiateDesktop(IPropertyLibrary propertyLibrary, AutomationBase automationBase)
    {
      AtafLog.Info();
      if (AtafDesktopApplicationFactory == null)
      {
        AtafConditionFactory =new ConditionFactory(propertyLibrary);
        AtafDesktopApplicationFactory = new AtafDesktopApplicationFactory();
        AtafDesktopWindowFactory.AtafAutomationBase = automationBase;
        AtafDesktopWindowFactory=new AtafDesktopWindowFactory();
        AtafDesktopGetElementFactory=new AtafDesktopGetElementFactory();

        AtafDesktopOperateOnElement =new AtafDesktopOperateOnElement();
        AtafDesktopOperateOnElement.DesktopGetElementFactory = AtafDesktopGetElementFactory;
      }
    }
  }
}
