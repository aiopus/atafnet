﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;

namespace AtafDesktopGuiCore.FlaUiDesktopMgmt
{
  public class AtafDesktopGetElementFactory
  {

    public AutomationElement GetElementByPropertyCondition(Window window, PropertyCondition propertyCondition)
    {
      return window.FindFirstDescendant(cf =>propertyCondition);
    }
  }
}
