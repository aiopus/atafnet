﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using FlaUI.Core;
using FlaUI.Core.AutomationElements;
using FlaUI.UIA3;

namespace AtafDesktopGuiCore.FlaUiDesktopMgmt
{
  public class AtafDesktopWindowFactory
  {
    public static Application AtafApplication { get; set; }
    public static AutomationBase AtafAutomationBase { get; set; }

    public Window[] GetLaunchedApplicationMainWindow()
    {
      try
      {
        AtafApplication.WaitWhileBusy();
      }
      catch (Exception e)
      {
        AtafLog.Info(e.Message);
      }

      return AtafApplication.GetAllTopLevelWindows(AtafAutomationBase);
    }

    public Window GetWindowByTitle(string windowTitle)
    {

      return GetLaunchedApplicationMainWindow().First(w => w.Title.Equals(windowTitle));
    }


    public Window GetWindowByAutomationId(string automationId)
    {
      return GetLaunchedApplicationMainWindow().First(w => w.AutomationId.Equals(automationId));

    }

    public Window GetWindowByIndex(int windowIndex)
    {
      return GetLaunchedApplicationMainWindow()[windowIndex];
    }

    public Window GetWindowOnDesktopByName(string windowTitle)
    {
      Window[] windows = AtafApplication.GetAllTopLevelWindows(AtafAutomationBase);
      return windows.First(w => w.Title.Contains(windowTitle));
    }

  }
}
