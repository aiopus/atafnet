﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;

namespace AtafDesktopGuiCore.FlaUiDesktopMgmt
{
  public class AtafDesktopOperateOnElement
  {
    public AtafDesktopGetElementFactory DesktopGetElementFactory { get; set; }

    public void InsertInToTextBoxByPropertyCondition(Window window, PropertyCondition propertyCondition, string textToInsert)
    {
      DesktopGetElementFactory.GetElementByPropertyCondition(window, propertyCondition).AsTextBox().Text = textToInsert;
    }
    public void ClickButtonByPropertyCondition(Window window, PropertyCondition propertyCondition)
    {
      DesktopGetElementFactory.GetElementByPropertyCondition(window, propertyCondition).AsButton().Click();
    }
  }
}
