﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtafCommonCore.AtafLogMgmt;
using FlaUI.Core;

namespace AtafDesktopGuiCore.FlaUiDesktopMgmt
{
  public class AtafDesktopApplicationFactory
  {
    public Application AtafApplication { get; set; }

    public Application LaunchApplication(string pathToApplication)
    {
      AtafApplication= FlaUI.Core.Application.Launch(pathToApplication);
      AtafLog.Info(pathToApplication);
      AtafDesktopWindowFactory.AtafApplication = AtafApplication;
      AtafLog.RunningApplication = AtafApplication;
      return AtafApplication;
    }

    public Application AttachOrLuanchApplication(ProcessStartInfo processStartInfo)
    {
      AtafApplication = FlaUI.Core.Application.AttachOrLaunch(processStartInfo);
      AtafDesktopWindowFactory.AtafApplication = AtafApplication;
      return AtafApplication;
    }

    public Application AttachToRunningApplication(string executable, int executableIndex=0)
    {
      AtafApplication = FlaUI.Core.Application.Attach(executable, executableIndex);
      AtafDesktopWindowFactory.AtafApplication = AtafApplication;
      return AtafApplication;
    }
    public Application AttachToRunningApplication(int processId)
    {
      AtafApplication = FlaUI.Core.Application.Attach(processId);
      AtafDesktopWindowFactory.AtafApplication = AtafApplication;
      return AtafApplication;
    }

    public void CloseApplication()
    {
      AtafApplication.Close();
      AtafApplication.Kill();
      AtafApplication.Dispose();
    }
  }
}
